ElastiStor BST Automation framework Usage


Description:

        BST automation framework has been developed to qualify the basic sanity test scenarios on ElastiStor. This is the Python based automation Framework utilizing ElastiStor REST APIs to 
perform various activities on ElastiStor.

Pre-requisites:

    1) Create a linux client (preferably CentOS7) with all basic development tools includeing python.Execute the following commands to install some important packages (if not already installed). This should be installed on the box where the tests are launched - and also can act as the I/O client during the tests. 

yum install openssl-devel
yum install python-devel
yum install iscsi-initiator-utils
yum install nfs-utils nfs-utils-lib
yum install java-1.8.0-openjdk
yum install csh
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install --upgrade setuptools
pip install requests
pip install paramiko
pip install pycrypto
pip install ecdsa
pip install numpy
  
   2) Setup the data network(VSM) connectivity on the linux client.

Usage:

Clone the repo:
                 git clone https://github.com/cloudbytestorage/devops.git
Navigate to the test directory.
                 cd devops/automation


Running MPTS

Navigate to MPTS directory.
Update smoketest.csv with your requirements.
Trigger the MPTS tests(Including config creation and functional tests) by executing all.sh
                 sh all.sh
The test results will be updated to results/result.csv

Running standard workloads

Navigate to standard_workloads directory.
Update conf.txt with your test requirements.
Run the standard workload tests on both NFS and iSCSI volumes by executing
    sh standared_workload_suite.sh
  
Running non-standard workloads

Navigate to nonstandard_workloads directory.
Update conf.txt with your test requirements.
Run the standard workload tests on both NFS and iSCSI volumes by executing
    sh nonstandard_workload_suite.sh

